<?php

namespace App\DataFixtures;

use App\Entity\Profile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

/**
 * Class AppFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @var array
     */
    private static $cvvs = [
        123,
        456,
        789,
    ];

    public function load(ObjectManager $manager): void
    {
        /** @var Generator */
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $profile = new Profile();
            $profile->setProfileNumber($faker->randomDigitNotNull);
            $profile->setType($faker->word);
            $profile->setName($faker->firstName);
            $profile->setSurname($faker->lastName);
            $profile->setAddress($faker->address);
            $profile->setCardNumber($faker->creditCardNumber);
            $profile->setCvv($faker->randomElement(self::$cvvs));
            $manager->persist($profile);
        }

        $manager->flush();
    }
}
