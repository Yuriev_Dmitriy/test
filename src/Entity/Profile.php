<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ProfileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     itemOperations={
 *          "get",
 *          "patch"={"normalization_context"={"groups"={Profile::PATCH_DENORMALIZATION_CONTEXT}}},
 *          "delete"
 *     },
 *     normalizationContext={"groups"={Profile::NORMALIZATION_CONTEXT}},
 *     denormalizationContext={"groups"={Profile::POST_DENORMALIZATION_CONTEXT}}
 * )
 * @ApiFilter(OrderFilter::class, properties={"type": "ASC"})
 * @ApiFilter(SearchFilter::class, properties={"type": "partial"})
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Profile
{
    public const
        NORMALIZATION_CONTEXT = "read",
        POST_DENORMALIZATION_CONTEXT = "post",
        PATCH_DENORMALIZATION_CONTEXT = "patch"
    ;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({Profile::NORMALIZATION_CONTEXT})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     */
    private $profileNumber;

    /**
     * @ORM\Column(type="text")
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::PATCH_DENORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="50")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::PATCH_DENORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="50")
     */
    private $surname;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::PATCH_DENORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=16)
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     * @Assert\Length(max="16")
     */
    private $cardNumber;

    /**
     * @ORM\Column(type="integer")
     * @Groups({Profile::NORMALIZATION_CONTEXT, Profile::POST_DENORMALIZATION_CONTEXT})
     * @Assert\NotBlank()
     * @Assert\Length(max="3")
     */
    private $cvv;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({Profile::NORMALIZATION_CONTEXT})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({Profile::NORMALIZATION_CONTEXT})
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfileNumber(): ?int
    {
        return $this->profileNumber;
    }

    public function setProfileNumber(int $profileNumber): self
    {
        $this->profileNumber = $profileNumber;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCardNumber(): ?int
    {
        return $this->cardNumber;
    }

    public function setCardNumber(int $cardNumber): self
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    public function getCvv(): ?int
    {
        return $this->cvv;
    }

    public function setCvv(int $cvv): self
    {
        $this->cvv = $cvv;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime());
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime());
        }
    }
}
