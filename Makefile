#######
.PHONY: install
#######

install: install-database

install-database: database-schema database-validate database-fixtures

database-create:
	@$(EXEC) docker-compose exec php-fpm php bin/console doctrine:database:create --connection=default --if-not-exists

database-schema:
	@$(EXEC) docker-compose exec php-fpm php bin/console doctrine:migrations:migrate --no-interaction

database-validate: database-schema
	@$(EXEC) docker-compose exec php-fpm php bin/console doctrine:schema:validate

database-fixtures: database-validate
	@$(EXEC) docker-compose exec php-fpm php bin/console doctrine:fixtures:load --no-interaction

#######
.PHONY: console
#######

console:
	@$(EXEC) docker-compose exec php-fpm sh

#######
.PHONY: utest
#######

utest:
	@$(EXEC) docker-compose exec php-fpm php bin/phpunit
