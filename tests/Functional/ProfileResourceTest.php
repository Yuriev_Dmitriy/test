<?php

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

/**
 * Class ProfileResourceTest
 */
class ProfileResourceTest extends ApiTestCase
{
    /**
     * @var array
     */
    private static $correctProfile = [
        'profileNumber' => 1,
        'type' => 'test type',
        'name' => 'test name',
        'surname' => 'test surname',
        'address' => 'test address',
        'cardNumber' => '1234567890123456',
        'cvv' => 123,
    ];

    /**
     * @var array
     */
    private static $incorrectProfile = [
        'profileNumber' => 1,
        'type' => 'test type',
        'name' => 'test name',
        'surname' => 'test surname',
        'address' => 'test address',
        'cardNumber' => '123456789012345612',
        'cvv' => 123456,
    ];

    public function testCreateProfile()
    {
        $client = self::createClient();

        $client->request('POST', '/api/profiles', [
            'json' => [],
        ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('POST', '/api/profiles', [
            'json' => self::$incorrectProfile,
        ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('POST', '/api/profiles', [
            'json' => self::$correctProfile,
        ]);
        $this->assertResponseStatusCodeSame(201);
    }
}
