Build project
```
docker-compose build
```
Run project
```
docker-compose up -d
```
Composer install
```
docker-compose exec php-fpm composer install
```
Install database
```
make install
```
Run tests
```
make utest
```
